package com.trial.akordirect.ui.list_machine

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.trial.akordirect.R
import com.trial.akordirect.core.Status
import com.trial.akordirect.databinding.FragmentMachineBinding
import org.koin.androidx.viewmodel.ext.android.viewModel

class ListMachineFragment : Fragment() {

    private lateinit var binding: FragmentMachineBinding
    private val listMachineViewModel: ListMachineViewModel by viewModel()

    private val machineAdapter: MachineAdapter by lazy {
        MachineAdapter {
            findNavController().navigate(
                ListMachineFragmentDirections
                    .actionListMachineToMachineDetails(it)
            )
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_machine, container, false)
        binding.rvMachinePhoto.apply {
            adapter = machineAdapter
        }

        listMachineViewModel.machineList.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    machineAdapter.setData(it.data!!)
                }
                Status.ERROR -> {
                    Toast.makeText(context, "Error", Toast.LENGTH_LONG).show()
                }
                Status.LOADING -> {
                    Toast.makeText(context, "Loading", Toast.LENGTH_LONG).show()
                }
            }
        })
        return binding.root
    }
}

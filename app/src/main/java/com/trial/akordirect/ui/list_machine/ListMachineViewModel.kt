package com.trial.akordirect.ui.list_machine


import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.trial.akordirect.domain.GetAllMachineUseCase

class ListMachineViewModel(private val getAllMachineUseCase: GetAllMachineUseCase) : ViewModel() {

    val machineList = liveData {
        emit(getAllMachineUseCase.execute())

    }
}
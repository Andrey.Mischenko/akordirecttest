package com.trial.akordirect.ui.machine_details


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import coil.api.load
import com.trial.akordirect.databinding.FragmentMachineDetailsBinding
import com.trial.akordirect.models.MachineShortInformation


class MachineDetailsFragment : Fragment() {

    private lateinit var binding: FragmentMachineDetailsBinding
    private val args: MachineDetailsFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentMachineDetailsBinding.inflate(inflater, container, false)
        setContent(args.machineArg)
        return binding.root
    }


    private fun setContent(content: MachineShortInformation) {
        binding.apply {
            ivMainImage.load(content.imageUrl)
            tvTypeMachine.text = content.type
            tvModelHeader.text = content.model
            tvManufacturer.text = content.manufacturer
            tvYear.text = content.year.toString()
            tvLocation.text = content.machineLocation
            tvMachineCondition.text = content.machinecondition
        }
    }
}

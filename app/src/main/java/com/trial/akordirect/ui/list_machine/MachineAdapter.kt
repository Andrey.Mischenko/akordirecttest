package com.trial.akordirect.ui.list_machine

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.Adapter
import coil.api.load
import com.trial.akordirect.databinding.GridViewItemMachineBinding
import com.trial.akordirect.models.MachineShortInformation

class MachineAdapter(private val onViewClick: (MachineShortInformation) -> Unit) :
    Adapter<MachineAdapter.ViewHolder>() {

    private var data: List<MachineShortInformation> = emptyList()

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        return ViewHolder(
            GridViewItemMachineBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int = data.size


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val value = data[position]
        holder.bind(value)
        holder.itemView.setOnClickListener {
            onViewClick(value)
        }
    }


    fun setData(words: List<MachineShortInformation>) {
        data = words
        notifyDataSetChanged()
    }

    companion object {
        private const val TAG = "MachineAdapter"
    }

    inner class ViewHolder(private val view: GridViewItemMachineBinding) :
        RecyclerView.ViewHolder(view.root) {
        fun bind(machine: MachineShortInformation) {
            view.ivMachinePhoto.load(machine.imageUrl)
            view.tvCondition.text = machine.machinecondition
            view.tvModel.text = machine.model
            view.tvTypeMachine.text = machine.type
        }
    }




}




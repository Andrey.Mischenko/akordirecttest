package com.trial.akordirect.models

import com.trial.akordirect.core.BaseMapper

class MachineMapper : BaseMapper<List<MachineTools>, List<MachineShortInformation>> {
    override fun map(from: List<MachineTools>): List<MachineShortInformation> {
        return from.map {
            MachineShortInformation(
                id = it.id,
                imageUrl = it.imageUrl,
                type = it.type,
                model = it.model,
                manufacturer = it.manufacturer,
                year = it.year,
                machineLocation = it.machinelocation,
                machinecondition = it.machinecondition
            )
        }
    }
}
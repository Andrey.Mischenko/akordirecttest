package com.trial.akordirect.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class MachineShortInformation(
    val id: Int,
    val imageUrl : String,
    val type: String,
    val model: String,
    val manufacturer: String,
    val year: Int,
    val machineLocation : String,
    val machinecondition: String
) : Parcelable
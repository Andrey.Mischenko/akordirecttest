package com.trial.akordirect.models

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass


@JsonClass(generateAdapter = true)
data class MachineTools(
    val axiscount: String,
    val cncsystem: String,
    val cncsystemfull: String,
    val country: String,
    val countryru: String,
    val id: Int,
    val info1en: String,
    val info2ru: String,
    val machinecondition: String,
    val machineconditionru: String,
    val machinelocation: String,
    val machinelocationru: String,
    val machineruntime: String,
    val manufacturer: String,
    val model: String,
    val modelurl: String,
    val photo: String,
    val photoVmcList: List<Any>,
    val positioningaccuracy: String,
    val price: Int,
    val productid: String,
    val sold: String,
    val spindlenose: String,
    val spindlepower: String,
    val spindleruntime: String,
    val spindlespeed: Int,
    val tablerectangleload: Int,
    val tableroundload: Int,
    val tableroundsize: String,
    val toolchangingchiptochip: String,
    val toolchangingtooltotool: String,
    val toolcount: Int,
    val toolmaxdiameter: Int,
    val toolmaxlength: Int,
    val toolmaxweight: Int,
    val type: String,
    val typeru: String,
    val typeurl: String,
    @Json(name = "video1")
    val imageUrl: String,
    val video2: String,
    val x: Int,
    val xtable: Int,
    val y: Int,
    val year: Int,
    val year1: Int,
    val ytable: Int,
    val z: Int
)
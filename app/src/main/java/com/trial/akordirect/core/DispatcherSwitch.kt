package com.trial.akordirect.core

import kotlinx.coroutines.Dispatchers

class DispatcherSwitch {
    fun main() = Dispatchers.Main
    fun default() = Dispatchers.Default
    fun unconfined() = Dispatchers.Unconfined
    fun io() = Dispatchers.IO
}
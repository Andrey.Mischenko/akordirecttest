package com.trial.akordirect.core

enum class ErrorCodes(val code: Int) {
    SocketTimeOut(-1),
    Unauthorised(401),
    NotFound(404)
}
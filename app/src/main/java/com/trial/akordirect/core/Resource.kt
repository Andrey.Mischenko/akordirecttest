package com.trial.akordirect.core

import com.trial.akordirect.core.Status.ERROR
import com.trial.akordirect.core.Status.LOADING
import com.trial.akordirect.core.Status.SUCCESS



data class Resource<out T>(val status: Status, val data: T?, val message: String?) {
    companion object {
        fun <T> success(data: T?): Resource<T> {
            return Resource(SUCCESS, data, null)
        }

        fun <T> error(msg: String, data: T?): Resource<T> {
            return Resource(ERROR, data, msg)
        }

        fun <T> loading(data: T?): Resource<T> {
            return Resource(LOADING, data, null)
        }
    }
}
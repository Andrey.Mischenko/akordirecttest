package com.trial.akordirect.core

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}
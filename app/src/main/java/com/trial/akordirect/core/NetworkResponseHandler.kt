package com.trial.akordirect.core

import retrofit2.HttpException
import java.lang.Exception
import java.net.SocketTimeoutException


open class NetworkResponseHandler {
    fun <T : Any> handleSuccess(data: T): Resource<T> {
        return Resource.success(data)
    }

    fun handleException(e: Exception): Resource<Nothing> {
        return when (e) {
            is HttpException -> Resource.error(getErrorMessage(e.code()), null)
            is SocketTimeoutException -> Resource.error(
                getErrorMessage(ErrorCodes.SocketTimeOut.code),
                null
            )
            else -> Resource.error(getErrorMessage(Int.MAX_VALUE), null)
        }
    }

    private fun getErrorMessage(code: Int): String {
        return when (code) {
            ErrorCodes.SocketTimeOut.code -> "Timeout"
            ErrorCodes.Unauthorised.code -> "Unauthorised"
            ErrorCodes.NotFound.code -> "Not found"
            else -> "Something went wrong"
        }
    }
}
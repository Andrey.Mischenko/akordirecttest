package com.trial.akordirect.data.repository

import com.trial.akordirect.core.Resource
import com.trial.akordirect.data.datasource.MachineDataSource
import com.trial.akordirect.domain.MachineRepository
import com.trial.akordirect.models.MachineTools

class MachineRepositoryImpl(private val dataSource: MachineDataSource) : MachineRepository {
    override suspend fun getMachine(): Resource<List<MachineTools>> {
        return dataSource.getMachineTools()
    }
}
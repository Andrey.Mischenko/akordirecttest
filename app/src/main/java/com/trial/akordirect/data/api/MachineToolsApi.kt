package com.trial.akordirect.data.api

import com.trial.akordirect.models.MachineTools
import retrofit2.http.GET

interface MachineToolsApi {
    @GET("vmc_json")
    suspend fun getMachineList(): List<MachineTools>
}
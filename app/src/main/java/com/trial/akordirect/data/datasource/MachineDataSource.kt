package com.trial.akordirect.data.datasource

import com.trial.akordirect.core.DispatcherSwitch
import com.trial.akordirect.core.Resource
import com.trial.akordirect.core.NetworkResponseHandler
import com.trial.akordirect.data.api.MachineToolsApi
import com.trial.akordirect.models.MachineTools
import kotlinx.coroutines.withContext
import java.lang.Exception

class MachineDataSource(
    private val api: MachineToolsApi,
    private val response: NetworkResponseHandler,
    private val dispatcher: DispatcherSwitch
) {


    suspend fun getMachineTools(): Resource<List<MachineTools>> {
        return withContext(dispatcher.io()) {
            return@withContext try {
                response.handleSuccess(api.getMachineList())
            } catch (e: Exception) {
                response.handleException(e)
            }
        }
    }

}
package com.trial.akordirect.di

import com.trial.akordirect.domain.GetAllMachineUseCase
import com.trial.akordirect.models.MachineMapper
import org.koin.dsl.module

private fun machineMapperProvider(): MachineMapper = MachineMapper()

private val useCaseModule = module {
    factory { GetAllMachineUseCase(repository = get(), machineMapper = machineMapperProvider()) }
}

val domain = listOf(useCaseModule)
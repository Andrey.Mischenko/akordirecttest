package com.trial.akordirect.di

import com.trial.akordirect.core.DispatcherSwitch
import com.trial.akordirect.core.NetworkResponseHandler
import com.trial.akordirect.data.api.MachineToolsApi
import com.trial.akordirect.data.datasource.MachineDataSource
import com.trial.akordirect.data.repository.MachineRepositoryImpl
import com.trial.akordirect.domain.MachineRepository
import com.squareup.moshi.Moshi
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

private const val URL_BASE = "http://q11.jvmhost.net/"

private fun loggerInterceptor(): HttpLoggingInterceptor =
    HttpLoggingInterceptor().apply { setLevel(HttpLoggingInterceptor.Level.BODY) }


private fun provideClient(): OkHttpClient = OkHttpClient()
    .newBuilder()
    .addInterceptor(loggerInterceptor())
    .build()

private fun provideMoshi() = Moshi.Builder().build()

private fun provideRetrofitInstance(okHttpClient: OkHttpClient, moshi: Moshi): Retrofit =
    Retrofit.Builder()
        .baseUrl(URL_BASE)
        .client(okHttpClient)
        .addConverterFactory(MoshiConverterFactory.create(moshi))
        .build()

private val retrofitModule = module {
    factory { provideClient() }
    factory { provideMoshi() }
    single { provideRetrofitInstance(okHttpClient = get(), moshi = get()) }
}

private fun provideMachineApi(retrofit: Retrofit): MachineToolsApi =
    retrofit.create(MachineToolsApi::class.java)

private fun providerNetworkResponseHandler(): NetworkResponseHandler =
    NetworkResponseHandler()

private fun providerDispatcher(): DispatcherSwitch = DispatcherSwitch()

private val apiModule = module {
    factory { provideMachineApi(retrofit = get()) }
}

private val dataSourceModule = module {
    single {
        MachineDataSource(
            api = get(),
            response = providerNetworkResponseHandler(),
            dispatcher = providerDispatcher()
        )
    }
}

private val repositoryModule = module {
    single<MachineRepository> {
        MachineRepositoryImpl(
            dataSource = get()
        )
    }
}

val data = listOf(retrofitModule, apiModule, dataSourceModule, repositoryModule)
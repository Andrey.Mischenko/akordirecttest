package com.trial.akordirect.di

import com.trial.akordirect.ui.list_machine.ListMachineViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

private val viewModelModule = module {
    viewModel { ListMachineViewModel(getAllMachineUseCase = get()) }
}

val presentation = listOf(viewModelModule)
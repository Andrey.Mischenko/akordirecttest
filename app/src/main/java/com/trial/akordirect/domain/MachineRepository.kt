package com.trial.akordirect.domain

import com.trial.akordirect.core.Resource
import com.trial.akordirect.models.MachineTools

interface MachineRepository {
    suspend fun getMachine() : Resource<List<MachineTools>>
}
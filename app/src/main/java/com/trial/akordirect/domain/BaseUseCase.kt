package com.trial.akordirect.domain

import com.trial.akordirect.core.Resource


interface BaseUseCase<T : Any> {
    suspend fun execute(): Resource<T>
}

interface BaseUseCaseWithParam<in P : Any, T : Any> {
   suspend fun execute(param: P): Resource<T>
}


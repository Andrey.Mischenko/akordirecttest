package com.trial.akordirect.domain

import com.trial.akordirect.core.Resource
import com.trial.akordirect.core.Status.*
import com.trial.akordirect.models.MachineMapper
import com.trial.akordirect.models.MachineShortInformation

class GetAllMachineUseCase(
    private val repository: MachineRepository,
    private val machineMapper: MachineMapper
) :
    BaseUseCase<List<MachineShortInformation>> {
    override suspend fun execute(): Resource<List<MachineShortInformation>> {
        val result = repository.getMachine()
        return when (result.status) {
            SUCCESS -> Resource.success(machineMapper.map(result.data!!))
            ERROR -> Resource.error("Error -> ${result.message}", null)
            LOADING -> Resource.loading(null)
        }
    }
}